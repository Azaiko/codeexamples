import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time

start_time = time.time()

# Set the graph titles and filenames based on questionaire
titles = ["De club heeft moderne apparatuur",
          "Het pand ziet er visueel aantrekkelijk uit",
          "De medewerkers zien er netjes uit",
          "De documenten rondom de dienstverlening zien er netjes uit",
          "Als Club Pellikaan belooft voor een bepaalde tijd iets te doen. dan gebeurt het ook",
          "Als er een probleem is wordt er oprechte belangstelling getoond in het oplossen van het probleem",
          "Club Pellikaan doet alles de eerste keer goed",
          "De groepslessen en andere activiteiten worden altijd op het beloofde tijdstip uitgevoerd",
          "Club Pellikaan besteedt moeite aan een correcte administratie",
          "Medewerkers geven precies aan wanneer er een groepsles. afspraak of andere activiteit is",
          "Medewerkers geven een goede service",
          "Medewerkers zijn altijd bereid om te helpen",
          "Medewerkers zijn nooit te druk om de reageren op verzoeken",
          "Het gedrag van de medewerkers geeft een gevoel van zekerheid",
          "Je hebt een goed gevoel over de omgang met de club",
          "De medewerkers zijn altijd beleefd",
          "De medewerkers beschikken over genoeg kennis om vragen te kunnen beantwoorden",
          "Je krijgt genoeg individuele aandacht",
          "De openingstijden zijn goed ingedeeld",
          "De medewerkers geven persoonlijke aandacht",
          "Club Pellikaan zet zich in voor jou belang om te sporten",
          "De medewerkers begrijpen jou specifieke behoeften",
          "Je maakt deel uit van een club met gelijkgestemde mensen"]
filename = ["moderne_apparatuur", "visueel_pand", "nette_werknemers", "nette_documenten",
          "nakomen_beloften", "oprecht_belangstelling_problemen", "een_keer_goed", "groepslessen_op_tijd",
          "correcte_administratie", "werknemers_groepsles_plaatsvindt", "medewerkers_goede_service",
          "medewerkers_bereid_helpen", "medewerkers_nooit_druk", "gedrag_medewerkers_zekerheid",
          "goed_gevoel_omgang", "medewerkers_beleefd", "medewerkers_kennis",
          "genoeg_aandacht", "openingstijden_goed", "medewerkers_aandacht",
          "CP_belang_sporten", "medewerkers_begrip", "gelijkgestemden"]

# Set other titles
q1 = "Klantcijfer"
q2 = "Klantbelangrijk"
q3 = "Klantverwachting"
q4 = "Manager verwachting"
subtitles = ["Gemiddelde iedereen", "Gemiddelde man", "Gemiddelde vrouw", "Gemiddelde 18 t/m 24 jaar",
        "Gemiddelde 25 t/m 34 jaar", "Gemiddelde 35 t/m 44 jaar", "Gemiddelde 45 t/m 54 jaar", "Gemiddelde 55 t/m 64 jaar",
         "Korter dan een maand", "Langer dan drie maanden", "Langer dan een jaar", "Langer dan twee jaar"]
tit2 = "Klantverwachting en manager"

# Average for everyone
def avg_all_bar():
    ldfcol1 = 5
    time_now = time.time()

    for i in range(0,22):
        ldfcol1 = ldfcol1 + 4
        ldfcol2 = ldfcol1 + 1
        ldfcol3 = ldfcol1 + 2
        ldf = pd.read_csv('./Data/MH_leden.csv', header=0, sep=',', usecols=[ldfcol1, ldfcol2, ldfcol3])
        mdf = pd.read_csv('./Data/MH_managers.csv', header=0, sep=',', usecols=[i+1])

        tit0 = titles[i]
        tit1 = subtitles[0]

        l = ldf.as_matrix()
        l = l[~np.isnan(l[:, 0])]
        l = l[~np.isnan(l[:, 1])]
        l = l[~np.isnan(l[:, 2])]

        m = mdf.as_matrix()
        m = m[~np.isnan(m[:, 0])]

        lavg1 = np.average(l[:, 0])
        lavg2 = np.average(l[:, 1])
        lavg3 = np.average(l[:, 2])
        mavg1 = np.average(m[:, 0])

        x = [q1, q2, q3, q4]
        y = [lavg1, lavg2, lavg3, mavg1]

        #plt.subplot(121)
        plt.bar(x, y, width=1, color='rgyb')
        plt.title(tit1)
        plt.yticks([1,2,3,4,5,6,7])
        plt.ylim(0,7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        plt.suptitle(tit0)
        plt.subplots_adjust(wspace=.4, bottom=.15)

        plt.savefig(str('./ImgOut/All/gem_' + filename[i] + '.png'))
        plt.clf()

        ldf.to_csv('./DataOut/All/all_' + filename[i] + '_all_data.csv', sep=',')

        average = [x, y]
        avgdf = pd.DataFrame(average)
        avgdf.to_csv('./DataOut/All/all_' + filename[i] + '_avg_data.csv', sep=',')
    print("avg_all_bar: --- %s seconds ---" % (time.time() - time_now))

# Male / Female averages
def avg_mf_bar():
    ldfcol1 = 5
    time_now = time.time()

    for i in range(0, 22):
        mvcol = 1
        ldfcol1 = int(ldfcol1 + 4)
        ldfcol2 = int(ldfcol1 + 1)
        ldfcol3 = int(ldfcol1 + 2)

        ldf = pd.read_csv('./Data/MH_leden.csv', header=0, sep=',', usecols=[mvcol, ldfcol1, ldfcol2, ldfcol3])
        mdf = pd.read_csv('./Data/MH_managers.csv', header=0, sep=',', usecols=[i + 1])

        # Rename columns in case of inconsistency
        ldf_question = str(ldf.columns[1])
        ldf.set_axis(["Ben je man of vrouw", ldf_question, "Hoe belangrijk vind je dit", "In hoeverre had je dit verwacht"], axis='columns', inplace=True)

        tit0 = titles[i]
        titm = subtitles[1]
        titv = subtitles[2]

        subset =['Ben je man of vrouw', str(titles[i]), 'Hoe belangrijk vind je dit',
                                'In hoeverre had je dit verwacht']
        ldf = ldf.dropna(subset=subset)

        m = mdf.as_matrix()
        l = ldf.as_matrix()

        lm = l[l[:,0] == 'Man']
        lv = l[l[:, 0] == 'Vrouw']

        lmavg1 = np.mean(lm[:, 1])
        lmavg2 = np.mean(lm[:, 2])
        lmavg3 = np.mean(lm[:, 3])
        lvavg1 = np.mean(lv[:, 1])
        lvavg2 = np.mean(lv[:, 2])
        lvavg3 = np.mean(lv[:, 3])
        mavg1 = np.mean(m[:, 0])

        x = [q1, q2, q3, q4]
        y1 = [lmavg1, lmavg2, lmavg3, mavg1]

        plt.figure(figsize=(10, 5))

        plt.subplot(121)
        plt.bar(x, y1, width=1, color='rgyb')
        plt.title(titm)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        y2 = [lvavg1, lvavg2, lvavg3, mavg1]

        plt.subplot(122)
        plt.bar(x, y2, width=1, color='rgyb')
        plt.title(titv)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        plt.suptitle(tit0)
        plt.subplots_adjust(wspace=.4, bottom=.15)

        plt.savefig(str('./ImgOut/MF/mv_' + filename[i] + '.png'))
        plt.clf()

        ldf.to_csv('./DataOut/MF/mv_' + filename[i] + '_all_data.csv', sep=',')

        average = [x, ["man"], y1, ["vrouw"], y2]
        avgdf = pd.DataFrame(average)
        avgdf.to_csv('./DataOut/MF/mv_' + filename[i] + '_avg_data.csv', sep=',')
    print("avg_mf_bar: --- %s seconds ---" % (time.time() - time_now))

# Age group averages barchart
def avg_age_bar():
    ldfcol1 = 5
    time_now = time.time()

    for i in range(0, 22):
        agecol = 2
        ldfcol1 = int(ldfcol1 + 4)
        ldfcol2 = int(ldfcol1 + 1)
        ldfcol3 = int(ldfcol1 + 2)

        ldf = pd.read_csv('./Data/MH_leden.csv', header=0, sep=',', usecols=[agecol, ldfcol1, ldfcol2, ldfcol3])
        mdf = pd.read_csv('./Data/MH_managers.csv', header=0, sep=',', usecols=[i + 1])

        # Rename columns in case of inconsistency
        ldf_question = str(ldf.columns[1])
        ldf.set_axis(["Hoe oud ben je", ldf_question, "Hoe belangrijk vind je dit", "In hoeverre had je dit verwacht"], axis='columns', inplace=True)

        tit0 = titles[i]
        tit1824 = subtitles[3]
        tit2534 = subtitles[4]
        tit3544 = subtitles[5]
        tit4554 = subtitles[6]
        tit5564 = subtitles[7]

        subset =['Hoe oud ben je', str(titles[i]), 'Hoe belangrijk vind je dit',
                                'In hoeverre had je dit verwacht']
        ldf = ldf.dropna(subset=subset)

        m = mdf.as_matrix()
        l = ldf.as_matrix()

        l1824 = l[l[:, 0] == '18 t/m 24 jaar']
        l2534 = l[l[:, 0] == '25 t/m 34 jaar']
        l3544 = l[l[:, 0] == '35 t/m 44 jaar']
        l4554 = l[l[:, 0] == '45 t/m 54 jaar']
        l5564 = l[l[:, 0] == '55 t/m 64 jaar']

        l1824avg1 = np.mean(l1824[:, 1])
        l1824avg2 = np.mean(l1824[:, 2])
        l1824avg3 = np.mean(l1824[:, 3])
        l2534avg1 = np.mean(l2534[:, 1])
        l2534avg2 = np.mean(l2534[:, 2])
        l2534avg3 = np.mean(l2534[:, 3])
        l3544avg1 = np.mean(l3544[:, 1])
        l3544avg2 = np.mean(l3544[:, 2])
        l3544avg3 = np.mean(l3544[:, 3])
        l4554avg1 = np.mean(l4554[:, 1])
        l4554avg2 = np.mean(l4554[:, 2])
        l4554avg3 = np.mean(l4554[:, 3])
        l5564avg1 = np.mean(l5564[:, 1])
        l5564avg2 = np.mean(l5564[:, 2])
        l5564avg3 = np.mean(l5564[:, 3])
        mavg1 = np.mean(m[:, 0])

        x = [q1, q2, q3, q4]
        y1 = [l1824avg1, l1824avg2, l1824avg3, mavg1]

        plt.figure(figsize=(15, 10))

        # plot 1
        plt.subplot(231)
        plt.bar(x, y1, width=1, color='rgyb')
        plt.title(tit1824)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        # plot 2
        y2= [l2534avg1, l2534avg2, l2534avg3, mavg1]

        plt.subplot(232)
        plt.bar(x, y1, width=1, color='rgyb')
        plt.title(tit2534)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        # plot 3
        y3 = [l3544avg1, l3544avg2, l3544avg3, mavg1]

        plt.subplot(233)
        plt.bar(x, y3, width=1, color='rgyb')
        plt.title(tit3544)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        # plot 4
        y4 = [l4554avg1, l4554avg2, l4554avg3, mavg1]

        plt.subplot(234)
        plt.bar(x, y4, width=1, color='rgyb')
        plt.title(tit4554)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        # plot 5
        y5 = [l5564avg1, l5564avg2, l5564avg3, mavg1]

        plt.subplot(235)
        plt.bar(x, y5, width=1, color='rgyb')
        plt.title(tit5564)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        plt.suptitle(tit0)
        plt.subplots_adjust(wspace=.4, bottom=.15)

        plt.savefig(str('./ImgOut/Age/bar_age_' + filename[i] + '.png'))
        plt.clf()

        ldf.to_csv('./DataOut/Age/age_' + filename[i] + '_all_data.csv', sep=',')

        average = [x, [tit1824], y1, [tit2534], y2, [tit3544], y3, [tit4554], y4, [tit5564], y5]
        avgdf = pd.DataFrame(average)
        avgdf.to_csv('./DataOut/Age/age_' + filename[i] + '_avg_data.csv', sep=',')
    print("avg_age_bar: --- %s seconds ---" % (time.time() - time_now))

# Age group averages linechart
def avg_age_line():
    ldfcol1 = 5
    time_now = time.time()

    for i in range(0, 22):
        agecol = 2
        ldfcol1 = int(ldfcol1 + 4)
        ldfcol2 = int(ldfcol1 + 1)
        ldfcol3 = int(ldfcol1 + 2)

        averages = []

        ldf = pd.read_csv('./Data/MH_leden.csv', header=0, sep=',', usecols=[agecol, ldfcol1, ldfcol2, ldfcol3])
        mdf = pd.read_csv('./Data/MH_managers.csv', header=0, sep=',', usecols=[i + 1])

        # Rename columns in case of inconsistency
        ldf_question = str(ldf.columns[1])
        ldf.set_axis(["Hoe oud ben je", ldf_question, "Hoe belangrijk vind je dit", "In hoeverre had je dit verwacht"], axis='columns', inplace=True)

        tit0 = titles[i]
        tit1824 = subtitles[3]
        tit2534 = subtitles[4]
        tit3544 = subtitles[5]
        tit4554 = subtitles[6]
        tit5564 = subtitles[7]

        subset =['Hoe oud ben je', str(titles[i]), 'Hoe belangrijk vind je dit',
                                'In hoeverre had je dit verwacht']
        ldf = ldf.dropna(subset=subset)

        m = mdf.as_matrix()
        l = ldf.as_matrix()

        l1824 = l[l[:, 0] == '18 t/m 24 jaar']
        l2534 = l[l[:, 0] == '25 t/m 34 jaar']
        l3544 = l[l[:, 0] == '35 t/m 44 jaar']
        l4554 = l[l[:, 0] == '45 t/m 54 jaar']
        l5564 = l[l[:, 0] == '55 t/m 64 jaar']

        l1824avg1 = np.mean(l1824[:, 1])
        l1824avg2 = np.mean(l1824[:, 2])
        l1824avg3 = np.mean(l1824[:, 3])
        l2534avg1 = np.mean(l2534[:, 1])
        l2534avg2 = np.mean(l2534[:, 2])
        l2534avg3 = np.mean(l2534[:, 3])
        l3544avg1 = np.mean(l3544[:, 1])
        l3544avg2 = np.mean(l3544[:, 2])
        l3544avg3 = np.mean(l3544[:, 3])
        l4554avg1 = np.mean(l4554[:, 1])
        l4554avg2 = np.mean(l4554[:, 2])
        l4554avg3 = np.mean(l4554[:, 3])
        l5564avg1 = np.mean(l5564[:, 1])
        l5564avg2 = np.mean(l5564[:, 2])
        l5564avg3 = np.mean(l5564[:, 3])
        mavg1 = np.mean(m[:, 0])

        x = [tit1824, tit2534, tit3544, tit4554, tit5564]
        y1 = [l1824avg1, l2534avg1, l3544avg1, l4554avg1, l5564avg1]
        y2 = [l1824avg2, l2534avg2, l3544avg2, l4554avg2, l5564avg2]
        y3 = [l1824avg3, l2534avg3, l3544avg3, l4554avg3, l5564avg3]
        y4 = [mavg1, mavg1, mavg1, mavg1, mavg1]

        plt.figure(figsize=(10, 10))

        # plot
        plt.plot(x, y1, 'r', x, y2, 'g', x, y3, 'y', x, y4, 'b', x, y1, 'ro', x, y2, 'go', x, y3, 'yo', x, y4, 'bo', linewidth='2.5')
        plt.title(tit0)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.legend([q1, q2, q3, q4])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        #plt.suptitle(tit0)

        plt.savefig(str('./ImgOut/Age/line_age_' + filename[i] + '.png'))
        plt.clf()
    print("avg_age_line: --- %s seconds ---" % (time.time() - time_now))

# Subscription type averages
def avg_sub_bar():
    ldfcol1 = 5
    time_now = time.time()

    for i in range(0, 22):
        agecol = 3
        ldfcol1 = int(ldfcol1 + 4)
        ldfcol2 = int(ldfcol1 + 1)
        ldfcol3 = int(ldfcol1 + 2)

        ldf = pd.read_csv('./Data/MH_leden.csv', header=0, sep=',', usecols=[agecol, ldfcol1, ldfcol2, ldfcol3])
        mdf = pd.read_csv('./Data/MH_managers.csv', header=0, sep=',', usecols=[i + 1])

        # Rename columns in case of inconsistency
        ldf_question = str(ldf.columns[1])
        ldf.set_axis(["Hoe lang ben je al lid", ldf_question, "Hoe belangrijk vind je dit", "In hoeverre had je dit verwacht"], axis='columns', inplace=True)

        tit0 = titles[i]
        titsub1 = subtitles[8]
        titsub2 = subtitles[9]
        titsub3 = subtitles[10]
        titsub4 = subtitles[11]

        subset =['Hoe lang ben je al lid', str(titles[i]), 'Hoe belangrijk vind je dit',
                                'In hoeverre had je dit verwacht']
        ldf = ldf.dropna(subset=subset)

        m = mdf.as_matrix()
        l = ldf.as_matrix()

        l1 = l[l[:, 0] == 'Korter dan een maand']
        l2 = l[l[:, 0] == 'Langer dan drie maanden']
        l3 = l[l[:, 0] == 'Langer dan een jaar']
        l4 = l[l[:, 0] == 'Langer dan twee jaar']

        l1avg1 = np.mean(l1[:, 1])
        l1avg2 = np.mean(l1[:, 2])
        l1avg3 = np.mean(l1[:, 3])
        l2avg1 = np.mean(l2[:, 1])
        l2avg2 = np.mean(l2[:, 2])
        l2avg3 = np.mean(l2[:, 3])
        l3avg1 = np.mean(l3[:, 1])
        l3avg2 = np.mean(l3[:, 2])
        l3avg3 = np.mean(l3[:, 3])
        l4avg1 = np.mean(l4[:, 1])
        l4avg2 = np.mean(l4[:, 2])
        l4avg3 = np.mean(l4[:, 3])
        mavg1 = np.mean(m[:, 0])

        x = [q1, q2, q3, q4]
        y1 = [l1avg1, l1avg2, l1avg3, mavg1]

        plt.figure(figsize=(15, 10))

        # plot 1
        plt.subplot(221)
        plt.bar(x, y1, width=1, color='rgyb')
        plt.title(titsub1)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        # plot 2
        y2 = [l2avg1, l2avg2, l2avg3, mavg1]

        plt.subplot(222)
        plt.bar(x, y2, width=1, color='rgyb')
        plt.title(titsub2)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        # plot 3
        y3 = [l3avg1, l3avg2, l3avg3, mavg1]

        plt.subplot(223)
        plt.bar(x, y3, width=1, color='rgyb')
        plt.title(titsub3)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        # plot 4
        y4 = [l4avg1, l4avg2, l4avg3, mavg1]

        plt.subplot(224)
        plt.bar(x, y4, width=1, color='rgyb')
        plt.title(titsub4)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        plt.suptitle(tit0)
        plt.subplots_adjust(wspace=.4, bottom=.15)

        plt.savefig(str('./ImgOut/Sub/bar_sub_' + filename[i] + '.png'))
        plt.clf()

        ldf.to_csv('./DataOut/Sub/sub_' + filename[i] + '_all_data.csv', sep=',')

        average = [x, [titsub1], y1, [titsub2], y2, [titsub3], y3, [titsub4], y4]
        avgdf = pd.DataFrame(average)
        avgdf.to_csv('./DataOut/Sub/sub_' + filename[i] + '_avg_data.csv', sep=',')
    print("avg_sub_bar: --- %s seconds ---" % (time.time() - time_now))

# Subscription type averages
def avg_sub_line():
    ldfcol1 = 5
    time_now = time.time()

    for i in range(0, 22):
        agecol = 3
        ldfcol1 = int(ldfcol1 + 4)
        ldfcol2 = int(ldfcol1 + 1)
        ldfcol3 = int(ldfcol1 + 2)

        averages = []

        ldf = pd.read_csv('./Data/MH_leden.csv', header=0, sep=',', usecols=[agecol, ldfcol1, ldfcol2, ldfcol3])
        mdf = pd.read_csv('./Data/MH_managers.csv', header=0, sep=',', usecols=[i + 1])

        # Rename columns in case of inconsistency
        ldf_question = str(ldf.columns[1])
        ldf.set_axis(["Hoe lang ben je al lid", ldf_question, "Hoe belangrijk vind je dit", "In hoeverre had je dit verwacht"], axis='columns', inplace=True)

        tit0 = titles[i]
        titsub1 = subtitles[8]
        titsub2 = subtitles[9]
        titsub3 = subtitles[10]
        titsub4 = subtitles[11]

        subset =['Hoe lang ben je al lid', str(titles[i]), 'Hoe belangrijk vind je dit',
                                'In hoeverre had je dit verwacht']
        ldf = ldf.dropna(subset=subset)

        m = mdf.as_matrix()
        l = ldf.as_matrix()

        l1 = l[l[:, 0] == 'Korter dan een maand']
        l2 = l[l[:, 0] == 'Langer dan drie maanden']
        l3 = l[l[:, 0] == 'Langer dan een jaar']
        l4 = l[l[:, 0] == 'Langer dan twee jaar']

        l1avg1 = np.mean(l1[:, 1])
        l1avg2 = np.mean(l1[:, 2])
        l1avg3 = np.mean(l1[:, 3])
        l2avg1 = np.mean(l2[:, 1])
        l2avg2 = np.mean(l2[:, 2])
        l2avg3 = np.mean(l2[:, 3])
        l3avg1 = np.mean(l3[:, 1])
        l3avg2 = np.mean(l3[:, 2])
        l3avg3 = np.mean(l3[:, 3])
        l3avg1 = np.mean(l4[:, 1])
        l4avg2 = np.mean(l4[:, 2])
        l4avg3 = np.mean(l4[:, 3])
        mavg1 = np.mean(m[:, 0])

        x = [titsub1, titsub2, titsub3, titsub4]
        y1 = [l1avg1, l2avg1, l3avg1, l3avg1]
        y2 = [l1avg2, l2avg2, l3avg2, l4avg2]
        y3 = [l1avg3, l2avg3, l3avg3, l4avg3]
        y4 = [mavg1, mavg1, mavg1, mavg1]

        plt.figure(figsize=(10, 10))

        # plot
        plt.plot(x, y1, 'r', x, y2, 'g', x, y3, 'y', x, y4, 'b', x, y1, 'ro', x, y2, 'go', x, y3, 'yo', x, y4, 'bo',
                 linewidth='2.5')
        plt.title(tit0)
        plt.yticks([1, 2, 3, 4, 5, 6, 7])
        plt.legend([q1, q2, q3, q4])
        plt.ylim(0, 7.5)
        plt.grid(True, linestyle='--', which='major', color='grey', alpha=.5)
        plt.ylabel("Gemiddeld cijfer")
        plt.xticks(rotation=10)

        #plt.suptitle(tit0)
        plt.subplots_adjust(wspace=.4, bottom=.15)

        plt.savefig(str('./ImgOut/Sub/line_sub_' + filename[i] + '.png'))
        plt.clf()

        #ldf.to_csv('./ImgOut/Sub/sub_' + filename[i] + '_data.csv', sep=',')
    print("avg_sub_line: --- %s seconds ---" % (time.time() - time_now))

# Get images for average
avg_all_bar()
avg_mf_bar()
avg_age_bar()
avg_age_line()
avg_sub_bar()
avg_sub_line()

print("--- Total time elapsed: %s seconds ---" % (time.time() - start_time))

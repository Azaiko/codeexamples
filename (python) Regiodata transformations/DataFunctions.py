import pyodbc
import pandas as pd
import pandas.io.sql as psql
import numpy as np

# Connects to a SQL database and executes a query. Returns pandas dataframe.
# Requires server, databaseName, userName, password and the query as input.
def runQuery(server, databaseName, userName, password, query):
    connectionString = str('DRIVER={SQL Server};SERVER=' + str(server) + ';DATABASE=' + str(databaseName) +
                           ';UID=' + str(userName) + ';PWD=' + str(password))
    cnxn = pyodbc.connect(connectionString)
    df = psql.read_sql(query, cnxn)
    return(df)

# Read either a text or csv file. Seperator for tabs is \t, encoding is most likely latin-1 or utf-8.
# headerRow can be Null. Returns pandas dataframe
# More documentation on read_csv https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_csv.html
def readFile(inputLocation, seperator, headerRow, encoding):
    df = pd.DataFrame(pd.read_csv(inputLocation, sep=seperator, header=headerRow, encoding=encoding))
    return(df)

# Requires dataframe input and columns to filter out. Returns new dataframe.
# Drops the specified column indexes
def dropColumns(dataFrame, columns):
    df = dataFrame.drop(dataFrame.columns[columns], axis=1)
    return(df)

# Only returns rows with a certain argument.
# Requires dataFrame, row in which to filter and argument to filter
# Possible to filter more values with dataFrame.loc[dataFrame['colnamex'] == val1 && dataFrame['colnamey'] == val2 etc.]
def filterRows(dataFrame, columnName, argument):
    df = dataFrame.loc[dataFrame[columnName] == argument]
    return(df)

# Legacy: Removes duplicate values from the dataframe
'''def removeDupes(dataFrame, subset):
    df = dataFrame.drop_duplicates(subset=subset, keep='first')
    return(df)'''

# Sums the values for columns where the indexes are specified. Indexes have to be a list of index strings.
def sumColumn(dataFrame, columns, index, colNames):
    dataFrame = dataFrame[columns]
    if (len(colNames) != len(dataFrame.columns.values)):
        colNames = dataFrame.columns.values
        colNames.append('')
    matrix = np.asmatrix(dataFrame.groupby(index))
    sum = np.asmatrix(dataFrame.groupby(index).sum())
    new =[]

    if (len(index) <= 1):
        smallIndex = True
    else:
        smallIndex = False

    for i in range(0, len(matrix) - 1):
        if smallIndex != True:
            firstItem = list(matrix[i][0,0])
            sumItem = int(sum[i])
            firstItem.append(sumItem)
            new.append(firstItem)
        else:
            item = []
            firstItem = matrix[i][0, 0]
            item.append(firstItem)
            sumItem = int(sum[i])
            item.append(sumItem)
            new.append(item)

    df = pd.DataFrame(new, columns=colNames)
    return(df)
'''def sumColumn(dataFrame, index, colNames):
    dataFrame = dataFrame
    if (len(colNames) != len(dataFrame.columns.values)):
        colNames = dataFrame.columns.values
        colNames.append('')
    matrix = np.asmatrix(dataFrame.groupby(index))
    sum = np.asmatrix(dataFrame.groupby(index).sum())
    new =[]

    if (len(index) <= 1):
        smallIndex = True
    else:
        smallIndex = False

    for i in range(0, len(matrix) - 1):
        if smallIndex != True:
            firstItem = list(matrix[i][0,0])
            sumItem = int(sum[i])
            firstItem.append(sumItem)
            new.append(firstItem)
        else:
            item = []
            firstItem = matrix[i][0, 0]
            item.append(firstItem)
            sumItem = int(sum[i])
            item.append(sumItem)
            new.append(item)

    df = pd.DataFrame(new, columns=colNames)
    return(df)'''

# Calculate the percentage between slice 1 / slice 2 * 100 if slice 1 and slice 2 are above certain threshold
# Slices should be [""SLICE1", "SLICE2", IDENTIFIER_COL"]
def calcPercentageBu(df, slices, threshold_1, threshold_2, newheader):
    df_slices = df[slices]
    matrix = np.asmatrix(df_slices)

    matrix_out = []

    for i in matrix:
        if i[0, 0] < threshold_1:
            matrix_out.append([0, i[0, 2]])
        elif i[0, 1] < threshold_2:
            matrix_out.append([0, i[0, 2]])
        else:
            outcome = i[0, 0] / i[0, 1] * 100
            matrix_out.append([outcome, i[0, 2]])

    columns = [newheader, df_slices.columns.values[2]]
    df_matrix_out = pd.DataFrame(matrix_out, columns=columns)

    df_out = pd.merge(df, df_matrix_out, left_on=columns[1], right_on=columns[1])

    return df_out
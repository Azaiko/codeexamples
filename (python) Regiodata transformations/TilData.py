import DataFunctions as fu
import pandas as pd

# Initialize variables
outputFileNameBuurtCurr = './Out/til_curr_bu.csv'
outputFileNameWijkCurr = './Out/til_curr_wk.csv'
outputFileNameBuurtCanc = './Out/til_canc_bu.csv'
outputFileNameWijkCanc = './Out/til_canc_wk.csv'
inputLocationPC6 = "D:/Google Drive/Club Pellikaan/Data/RegioData/2017 postcodes/PC6.csv"
inputLocationBuurtenKaart = "D:/Google Drive/Club Pellikaan/Data/RegioData/2017 buurtenkaart/buurt_2017.csv"
inputLocationKerncijfers = "D:/Google Drive/Club Pellikaan/Data/RegioData/2017 kerncijfers wijk en buurt/kwb-2017.csv"
inputLocationKerncijfers2015= "D:/Google Drive/Club Pellikaan/Data/RegioData/2015 kerncijfers wijk en buurt/kwb-2015.csv"
queryTilCurr = "SELECT REPLACE(HomePostCode, ' ', '') AS PostCode ,COUNT(REPLACE(HomePostCode, ' ', '')) AS CountPostcode FROM dbo.Members WHERE HomePostCode != '' AND (StatusID = 'CURR' OR StatusID = 'FROZ' OR StatusID = 'SUS' OR StatusID = 'SUSP') AND SiteID = 'TIL' GROUP BY REPLACE(HomePostCode, ' ', '')"
queryTilCanc = "SELECT REPLACE(HomePostCode, ' ', '') AS PostCode ,COUNT(REPLACE(HomePostCode, ' ', '')) AS CountPostcode FROM dbo.Members WHERE HomePostCode != '' AND (StatusID = 'CANC' OR StatusID = 'CANCE' OR StatusID = 'DEURW' OR StatusID = 'EXP') AND SiteID = 'TIL' GROUP BY REPLACE(HomePostCode, ' ', '')"
serverIp = ''
databaseName = ''
userName = ''
password = ''
gmCodeString = 'GM0855'
gmCodeInteger = 855

# Print column titles with df.columns
# Print shape with df.shape
outputFileName = [[outputFileNameBuurtCurr, outputFileNameWijkCurr], [outputFileNameBuurtCanc, outputFileNameWijkCanc]]
query = [queryTilCurr, queryTilCanc]

for i in range(0, 2):
    # Run a SQL script to retrieve a count for each postcode
    tilPostcodeCount = fu.runQuery(serverIp, databaseName, userName, password, query[i])

    # Get postcodes for Tilburg with buurtnaam
    filePC6 = fu.readFile(inputLocationPC6, ',', 0, 'utf-8')
    dataPC6 = fu.dropColumns(filePC6, [4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16])
    tilDataPC6 = fu.filterRows(dataPC6, 'gemeentecode', gmCodeInteger)

    # Get postcodes for Tilburg with buurtnaam
    fileBu = fu.readFile(inputLocationBuurtenKaart, ',', 0, 'utf-8')
    dataBu = fu.dropColumns(fileBu, [5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
                                     29, 30, 31, 32, 33, 34, 35, 36, 37, 38])
    tilDataBu = fu.filterRows(dataBu, 'GM_CODE', gmCodeString)

    # Join tilDataBu en tilDataPC6 on buurtnaam
    df = pd.merge(tilDataPC6, tilDataBu, left_on='cbs buurtnaam', right_on='BU_NAAM')
    df = pd.merge(df, tilPostcodeCount, left_on='pc6', right_on='PostCode')

    # Remove some columns that won't be needed anymore. This data will be added with 'wk en buurt statistieken'
    df = fu.dropColumns(df, [0, 1, 2, 3, 4, 8, 9, 11])

    # Sum the values for CountPostCode
    df = fu.sumColumn(df, ['BU_CODE', 'BU_NAAM', 'WK_CODE', 'AANT_INW'], ['BU_CODE', 'BU_NAAM', 'WK_CODE', 'BU_INW', 'BU_LEDEN'])

    # Set this as final df_bu
    df_bu = df

    # Load 'kerncijfers wijk en buurt 2017' and merge
    dataBu = fu.readFile(inputLocationKerncijfers, ',', 0, 'utf-8')
    # Add suffix to make it easier to distinguish BU and WK
    dataBu = dataBu.add_prefix('BU_')
    df_bu = pd.merge(df_bu, dataBu, left_on='BU_CODE', right_on='BU_gwb_code')
    # Export to csv
    df_bu.to_csv(outputFileName[i][0], encoding='utf-8', index=False)

    # Calculate members per wijk instead of buurt
    wk = fu.dropColumns(df, [0, 1, 3])
    wk = fu.sumColumn(wk, ['WK_CODE'], ['WK_CODE', 'WK_LEDEN'])
    df = pd.merge(df, wk, left_on='WK_CODE', right_on='WK_CODE')
    # Set this as final df_wk
    df_wk = df
    # Load 'kerncijfers wijk en buurt 2017' and merge
    dataWk = fu.readFile(inputLocationKerncijfers, ',', 0, 'utf-8')
    # Add suffix to make it easier to distinguish BU and WK
    dataWk = dataWk.add_prefix('WK_')
    df_wk = pd.merge(df_wk, dataWk, left_on='WK_CODE', right_on='WK_gwb_code')
    # Export to csv
    df_wk.to_csv(outputFileName[i][1], encoding='utf-8', index=False)

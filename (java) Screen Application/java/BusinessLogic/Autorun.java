package BusinessLogic;

/*
 * Copyright (c) 2017.     This file is part of Roscolr3.
 *
 * Roscolr3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Roscolr3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Roscolr3.  If not, see <http://www.gnu.org/licenses/>.
 */

import Presentation.Display;

/**
 * Created by Rodi on 01-Mar-17.
 */


/**
 * Current version: 3.2
 * */
public class Autorun {

    public static void main(String[] args) {
        System.out.println("Everything went great! Starting application now...");
        Thread t = new Thread(new Display(), "Display");
        t.start();
    }
}

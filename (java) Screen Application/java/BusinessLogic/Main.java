/*
 * Copyright (c) 2017.     This file is part of Roscolr3.
 *
 * Roscolr3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Roscolr3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Roscolr3.  If not, see <http://www.gnu.org/licenses/>.
 */

package BusinessLogic;

import DataAccess.ColorParser;
import DataAccess.ConfigParser;
import DataAccess.OpeningHourParser;
import Presentation.Display;
import org.joda.time.LocalDate;

import java.util.Scanner;


/**
 * Created by Rodi on 01-Mar-17.
 */


/**
 * Current version: 3.0
 * */
public class Main {

    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);
        System.out.println("\nMaak een keuze:" +
                "\nType 0 om een harde reset uit te voeren. Alles wordt opnieuw gegenereerd." +
                "\nType 1 om verder te gaan met alle bestaande settings." +
                "\nType 2 om het kleurenschema voor deze week te behouden en een nieuw schema voor volgende week te genereren." +
                "\nType 3 om te annuleren en af te sluiten." +
                "\nLET OP: Na een reset is het niet meer mogelijk om iets terug te draaien!");
        int n = reader.nextInt();

        if (n == 0){
            // Hard reset
            System.out.println("Input " + n + " has been accepted. \nDoing a hard reset, initializing variables...");
            ConfigParser configParser = new ConfigParser();
            OpeningHourParser openingHourParser = new OpeningHourParser();
            ColorScheduleGenerator colorScheduleGenerator = new ColorScheduleGenerator();
            ColorParser colorParser = new ColorParser();
            int weekStart = configParser.getGenerationDay();

            LocalDate date = new LocalDate();

            int aDay = date.getDayOfMonth();
            int aMonth = date.getMonthOfYear();
            int aYear = date.getYear();
            int dow = date.getDayOfWeek();

            if (weekStart > dow){
                date = date.plusDays(weekStart - dow - 1);
            } else if (weekStart < dow){
                date = date.plusDays(7 - dow + weekStart - 1);
            } else {
                date = date.plusDays(6);
            }

            int bDay = date.getDayOfMonth();
            int bMonth = date.getMonthOfYear();
            int bYear = date.getYear();
            date = date.plusDays(1);
            int cDay = date.getDayOfMonth();
            int cMonth = date.getMonthOfYear();
            int cYear = date.getYear();
            date = date.plusDays(6);
            int dDay = date.getDayOfMonth();
            int dMonth = date.getMonthOfYear();
            int dYear = date.getYear();

            System.out.println("Generating list one...");
            openingHourParser.generateOpeningHourXml(String.format("%02d-%02d-%02d", aYear, aMonth, aDay)
                    ,String.format("%02d-%02d-%02d", bYear, bMonth, bDay));
            colorParser.generateXml(colorScheduleGenerator.generateColorList());
            System.out.println("Sending out first email...");
            EmailSender emailSender = new EmailSender();
            emailSender.sendMailWithAttachment("Kleurenschema " + (Integer.toString(aDay)) +
                    "-" + Integer.toString(aMonth) + "-" + Integer.toString(aYear));
            colorParser.transferColors();
            System.out.println("Generating list two...");
            openingHourParser.generateOpeningHourXml(String.format("%02d-%02d-%02d", cYear, cMonth, cDay)
                    ,String.format("%02d-%02d-%02d", dYear, dMonth, dDay));
            colorParser.generateXml(colorScheduleGenerator.generateColorList());
            System.out.println("Sending out second email...");
            emailSender.sendMailWithAttachment("Kleurenschema " + (Integer.toString(cDay)) +
                    "-" + Integer.toString(cMonth) + "-" + Integer.toString(cYear));
            System.out.println("Everything went great! Starting application now...");
            Thread t = new Thread(new Display(), "Display");
            t.start();
        }
        else if (n == 1){
            // Continue with existing sources
            System.out.println("Everything went great! Starting application now...");
            Thread t = new Thread(new Display(), "Display");
            t.start();
        } else if (n == 2) {
            // "Soft reset"
            System.out.println("Input " + n + " has been accepted. \nGetting current color schema...");
            ConfigParser configParser = new ConfigParser();
            OpeningHourParser openingHourParser = new OpeningHourParser();
            ColorScheduleGenerator colorScheduleGenerator = new ColorScheduleGenerator();
            ColorParser colorParser = new ColorParser();
            EmailSender emailSender = new EmailSender();
            int weekStart = configParser.getGenerationDay();

            LocalDate date = new LocalDate();
            int dow = date.getDayOfWeek();

            if (weekStart > dow) date = date.plusDays(weekStart - dow - 1);
            else if (weekStart < dow){
                date = date.plusDays(7 - dow + weekStart - 1);
            } else {
                date = date.plusDays(6);
            }

            date = date.plusDays(1);
            int cDay = date.getDayOfMonth();
            int cMonth = date.getMonthOfYear();
            int cYear = date.getYear();
            date = date.plusDays(6);
            int dDay = date.getDayOfMonth();
            int dMonth = date.getMonthOfYear();
            int dYear = date.getYear();

            System.out.println("Generating colors for next week...");
            openingHourParser.generateOpeningHourXml(String.format("%02d-%02d-%02d", cYear, cMonth, cDay)
                    ,String.format("%02d-%02d-%02d", dYear, dMonth, dDay));
            colorParser.generateXml(colorScheduleGenerator.generateColorList());
            System.out.println("Sending out the email...");
            emailSender.sendMailWithAttachment("Kleurenschema " + (Integer.toString(cDay)) +
                    "-" + Integer.toString(cMonth) + "-" + Integer.toString(cYear));

            System.out.println("Everything went great! Starting application now...");
            Thread t = new Thread(new Display(), "Display");
            t.start();
        } else if (n == 3){
            System.out.println("De software is afgesloten.");
        } else {
            System.out.println("Foute input. De software sluit nu af.");
        }
    }
}

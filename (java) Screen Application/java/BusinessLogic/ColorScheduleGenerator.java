package BusinessLogic;

import BusinessEntities.ColorCode;
import BusinessEntities.ColorSchedule;
import BusinessEntities.OpeningHour;
import DataAccess.OpeningHourParser;
import org.joda.time.LocalDate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Rodi on 17-Mar-17.
 */
public class ColorScheduleGenerator {
    private List<ColorSchedule> colorScheduleList = new ArrayList<>();
    private List<ColorSchedule> scheduleList = new ArrayList<>();
    private List<OpeningHour> openingHours = new ArrayList<>();

    private final List<ColorCode> standardColors = new ArrayList<>();
    private List<ColorCode> currentColorArray = new ArrayList<>();
    private List<ColorCode> tempColorArray = new ArrayList<>();
    private List<ColorCode> fullColorArray = new ArrayList<>();

    private OpeningHour openingHour;
    private ColorSchedule schedule;
    private ColorSchedule colorSchedule;

    private LocalDate.Property pDoW;

    private String longDate;

    private int timeOpen;
    private int size;


    public ColorScheduleGenerator(){
        standardColors.add(new ColorCode("Blauw",new Color(0, 128, 255)));
        standardColors.add(new ColorCode("Geel",new Color(255, 255, 0)));
        standardColors.add(new ColorCode("Wit",new Color(255, 255, 255)));
        standardColors.add(new ColorCode("Oranje",new Color(255, 128, 0)));
        standardColors.add(new ColorCode("Groen",new Color(0, 153, 0)));

        size = standardColors.size();
    }

    /** Returns a schedule */
    public List<ColorSchedule> generateWeeklySchedule(){

        /** Add data from openinghours.xml to list of openinghour objects */
        try {
            System.out.println("Generating schedule from openinghours");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            File inputFile = new File("OpeningHours.xml");
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("day");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nNode = nodeList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    openingHour = new OpeningHour(LocalDate.parse(eElement.getElementsByTagName("date").item(0).getTextContent())
                            , Integer.parseInt(eElement.getElementsByTagName("openTime").item(0).getTextContent())
                            , Integer.parseInt(eElement.getElementsByTagName("closeTime").item(0).getTextContent())
                            , Integer.parseInt(eElement.getElementsByTagName("venueId").item(0).getTextContent())
                            , Integer.parseInt(eElement.getElementsByTagName("dayOfWeek").item(0).getTextContent()));
                }
                openingHours.add(openingHour);
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(OpeningHourParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        /** Use list of openinghour objects to generate a list with schedule */
        for (OpeningHour openingHour : openingHours){
            // Get the weekday as text, make the first character uppercase
            pDoW = openingHour.getDate().dayOfWeek();
            longDate = pDoW.getAsText(new Locale("NL"));
            longDate = longDate.substring(0,1).toUpperCase() + longDate.substring(1).toLowerCase();
            schedule = new ColorSchedule(openingHour.getDayOfWeek()
                    , longDate
                    , openingHour.getDate()
                    , openingHour.getOpenTime()
                    , openingHour.getCloseTime());
            scheduleList.add(schedule);
        }
        return scheduleList;
    }

    /** Generates a color list and finishes a schedule for the defined week */
    public List<ColorSchedule> generateColorList(){
        System.out.println("Running randomize color algorithm");
        colorScheduleList.clear();
        scheduleList = generateWeeklySchedule();

        for (ColorSchedule schedule : scheduleList){
            timeOpen = schedule.getCloseTime() - schedule.getOpenTime();

            fullColorArray.clear();
            currentColorArray.clear();

            currentColorArray.addAll(standardColors);
            Collections.shuffle(currentColorArray);

            if (timeOpen > 5) {
                for (int i = 0; i < timeOpen; i = i + size) {
                    tempColorArray.clear();
                    tempColorArray.addAll(currentColorArray);
                    Collections.shuffle(currentColorArray);
                    while ((tempColorArray.get(size - 1).getColor().equals(currentColorArray.get(0).getColor()))
                            | (tempColorArray.get(size - 1).getColor().equals(currentColorArray.get(1).getColor()))
                            | (tempColorArray.get(size - 2).getColor().equals(currentColorArray.get(0).getColor()))
                            | (tempColorArray.get(size - 2).getColor().equals(currentColorArray.get(1).getColor()))) {
                        Collections.shuffle(currentColorArray);
                    }
                    fullColorArray.addAll(currentColorArray);
                }
            } else {
                fullColorArray.addAll(currentColorArray);
            }

            for (int i = 0; i < timeOpen; i++){
                colorSchedule = new ColorSchedule(schedule.getDay()
                        ,schedule.getOpenTime()
                        ,schedule.getCloseTime()
                        ,schedule.getDayOfWeekLong()
                        ,schedule.getOpenTime() + i
                        ,Integer.toString(fullColorArray.get(i).getColor().getRGB())
                        ,fullColorArray.get(i).getName()
                        ,schedule.getDate());
                colorScheduleList.add(colorSchedule);
            }
        }
        openingHours.clear();
        scheduleList.clear();
        System.out.println("Finished randomizing, returning the list of colors");
        return colorScheduleList;
    }
}

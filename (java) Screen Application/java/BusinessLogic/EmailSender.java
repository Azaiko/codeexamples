package BusinessLogic;

import DataAccess.ConfigParser;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.List;
import java.util.Properties;

/**
 * Created by Rodi on 10-Apr-17.
 */
public class EmailSender {

    private final ConfigParser configParser = new ConfigParser();
    private final List<String> emailList = configParser.getEmailList();
    private final String body = "In de bijlage staat het kleurenschema voor de volgende week."
            + "\n\n"
            + "\nDeze email is automatisch gegenereerd.\nNeem voor vragen of opmerkingen contact op met de afdeling ICT.";
    private final String attachment = "Kleurenlijst.html";
    private final String from = "";
    private final String pass = "";

    /*
     * This function sends out an email with attachment
     */
    public void sendMailWithAttachment(String subject) {
        System.out.println("Starting email sending process");

        // Setting the host to gmail.
        String host = "smtp.gmail.com";

        Properties props = new Properties();

        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

        // Get the Session object.
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(from, pass);
                    }
                });

        try {
            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            for (String to : emailList)
                message.addRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            // Set Subject: header field
            message.setSubject(subject);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setText(body);

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            String filename = attachment;
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);

            System.out.println("Send email successfully.....");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}

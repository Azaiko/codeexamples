package DataAccess;

import BusinessEntities.ColorSchedule;
import BusinessLogic.ColorScheduleGenerator;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.awt.*;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Created by Rodi on 17-Mar-17.
 */
public class ColorParser {

    private int iterator = 0;
    private Element dayElement;

    private NodeList iNodeList;
    private NodeList jNodeList;
    private Document document;

    private int openingHourInt;
    private int closingHourInt;

    private Node iNode;
    private Node jNode;

    private Element iElement;
    private Element jElement;

    private Color color;

    /** Generate XML document with a new color Schedule */
    public void generateXml(List<ColorSchedule> colorScheduleList){
        try {
            System.out.println("Creating a schedule with colors");
            // creating the document
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element rootElement = doc.createElement("colorSchedule");
            doc.appendChild(rootElement);

            for (ColorSchedule colorSchedule : colorScheduleList) {
                if (iterator != colorSchedule.getDay()) {
                    dayElement = doc.createElement("dayElement");
                    rootElement.appendChild(dayElement);
                    iterator = colorSchedule.getDay();

                    //add children to element
                    Element date = doc.createElement("date");
                    date.appendChild(doc.createTextNode(colorSchedule.getDate().toString()));
                    dayElement.appendChild(date);
                    Element day = doc.createElement("day");
                    day.appendChild(doc.createTextNode(Integer.toString(colorSchedule.getDay())));
                    dayElement.appendChild(day);
                    Element openingHour = doc.createElement("openTime");
                    openingHour.appendChild(doc.createTextNode(Integer.toString(colorSchedule.getOpenTime())));
                    dayElement.appendChild(openingHour);
                    Element closingHour = doc.createElement("closeTime");
                    closingHour.appendChild(doc.createTextNode(Integer.toString(colorSchedule.getCloseTime())));
                    dayElement.appendChild(closingHour);
                    Element dayLong = doc.createElement("dayLong");
                    dayLong.appendChild(doc.createTextNode(colorSchedule.getDayOfWeekLong()));
                    dayElement.appendChild(dayLong);
                }
                //create element
                Element timeSlot = doc.createElement("timeslot");
                dayElement.appendChild(timeSlot);
                Element time = doc.createElement("time");
                time.appendChild(doc.createTextNode(Integer.toString(colorSchedule.getTime())));
                timeSlot.appendChild(time);
                Element color = doc.createElement("color");
                color.appendChild(doc.createTextNode(colorSchedule.getColorName()));
                timeSlot.appendChild(color);
                Element colorCode = doc.createElement("colorCode");
                colorCode.appendChild(doc.createTextNode(colorSchedule.getColor()));
                timeSlot.appendChild(colorCode);
            }
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("colors.xml"));
            transformer.transform(source, result);

            System.out.println("Generating html document");
            // converting xml to html
            Source xslDoc = new StreamSource("dailyColorList.xsl");
            Source xmlDoc = new StreamSource("colors.xml");
            OutputStream htmlFile = new FileOutputStream("Kleurenlijst.html");
            Transformer transformerXSL = transformerFactory.newTransformer(xslDoc);
            transformerXSL.transform(xmlDoc, new StreamResult(htmlFile));
        } catch (ParserConfigurationException | DOMException | TransformerException e) {
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ColorParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** Copy next weeks color list to this weeks color list */
    public void transferColors(){
        try {
            System.out.println("Transferring colors for display usage");
            File from = new File("colors.xml");
            File to = new File("colorsCurr.xml");
            Files.copy( from.toPath(), to.toPath(),REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Get a nodelist from the most recent colors.xml */
    private void loadDocument(){
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            File inputFile = new File("colorsCurr.xml");
            document = dBuilder.parse(inputFile);
            iNodeList = document.getElementsByTagName("dayElement");
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(OpeningHourParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** Returns the color for a day and time from the XML document */
    public Color getColor(int day, int time){
        System.out.println("Getting color for timeslot");
        loadDocument();
        for (int i = 0; i < iNodeList.getLength(); i++) {
            iNode = iNodeList.item(i);
            if (iNode.getNodeType() == Node.ELEMENT_NODE) {
                iElement = (Element) iNode;
                if (Integer.parseInt(iElement.getElementsByTagName("day").item(0).getTextContent()) == day) {
                    jNodeList = iElement.getElementsByTagName("timeslot");
                    for (int j = 0; j < jNodeList.getLength(); j++) {
                        jNode = jNodeList.item(j);
                        if (jNode.getNodeType() == Node.ELEMENT_NODE) {
                            jElement = (Element) jNode;
                            if (Integer.parseInt(jElement.getElementsByTagName("time").item(0).getTextContent()) == time) {
                                color = new Color(Integer.parseInt(jElement.getElementsByTagName("colorCode").item(0).getTextContent()));
                            }
                        }
                    }
                }
            }
        }
        return color;
    }

    /** Return the opening hour from the XML document for a specific dayOfWeek.*/
    public int getOpeningHour(int day) {
        System.out.println("Getting opening hour for current day");
        loadDocument();
        for (int temp = 0; temp < iNodeList.getLength(); temp++) {
            Node nNode = iNodeList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if (eElement.getElementsByTagName("day").item(0).getTextContent().equals(Integer.toString(day))) {
                    openingHourInt = Integer.parseInt(eElement.getElementsByTagName("openTime").item(0).getTextContent());
                    return openingHourInt;
                }
            }
        }
        return 0;
    }

    /** Return the closing hour from the XML document for a specific dayOfWeek. */
    public int getClosingHour(int day) {
        System.out.println("Getting closing hour for current day");
        loadDocument();
        for (int temp = 0; temp < iNodeList.getLength(); temp++) {
            Node nNode = iNodeList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if (eElement.getElementsByTagName("day").item(0).getTextContent().equals(Integer.toString(day))) {
                    closingHourInt = Integer.parseInt(eElement.getElementsByTagName("closeTime").item(0).getTextContent());
                    return closingHourInt;
                }
            }
        }
        return 0;
    }
}

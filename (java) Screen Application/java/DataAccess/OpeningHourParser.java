package DataAccess;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import BusinessEntities.OpeningTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

/**
 * @author Rodi
 */
public class OpeningHourParser {

    private OpeningTime openingTime;
    private List<OpeningTime> openingTimeList = new ArrayList<>();

    private RestConnector restConnector;
    private ConfigParser configParser;

    private DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");

    private String venueIdStr;

    public OpeningHourParser(){
        configParser = new ConfigParser();
        venueIdStr =configParser.getVenueId();
    }

    /** Generates the openinghours XML file with opening hours from REST service data */
    private List<OpeningTime> getOpeningTimesFromRest(String start, String end) {
        try {
            System.out.println("Building opening hours");
            restConnector = new RestConnector();
            openingTimeList.clear();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(restConnector.getDataFromRest(start,end)));
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("soap:Envelope");
            Node n = nodeList.item(0).getFirstChild().getFirstChild().getFirstChild().getFirstChild();
            NodeList timeList = n.getChildNodes();
            for (int i = 0; i < timeList.getLength(); i++) {
                Node nNode = timeList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    openingTime = new OpeningTime( eElement.getElementsByTagName("StartTime").item(0).getTextContent()
                            , eElement.getElementsByTagName("StartTime").item(0).getTextContent()
                            , eElement.getElementsByTagName("EndTime").item(0).getTextContent());
                }
                openingTimeList.add(openingTime);
            }

            for (OpeningTime openingTime : openingTimeList) {
                openingTime.setDate(openingTime.getDate().substring(0, 10));
                openingTime.setStartTime(openingTime.getStartTime().substring(11, 13));
                openingTime.setEndTime(openingTime.getEndTime().substring(11, 13));
            }
            return openingTimeList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /** Generation of opening hours XML */
    public void generateOpeningHourXml(String start, String end) {
        try {
            openingTimeList = getOpeningTimesFromRest(start,end);
            // creating the document
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element rootElement = doc.createElement("openingHour");
            doc.appendChild(rootElement);

            for (OpeningTime openingTime : openingTimeList) {
                Element dayElement = doc.createElement("day");
                rootElement.appendChild(dayElement);

                LocalDate dt = formatter.parseLocalDate(openingTime.getDate());

                //add children to element
                Element date = doc.createElement("date");
                date.appendChild(doc.createTextNode(openingTime.getDate()));
                dayElement.appendChild(date);
                Element openTime = doc.createElement("openTime");
                openTime.appendChild(doc.createTextNode(openingTime.getStartTime()));
                dayElement.appendChild(openTime);
                Element closeTime = doc.createElement("closeTime");
                closeTime.appendChild(doc.createTextNode(openingTime.getEndTime()));
                dayElement.appendChild(closeTime);
                Element dayOfWeek = doc.createElement("dayOfWeek");
                dayOfWeek.appendChild(doc.createTextNode(Integer.toString(dt.getDayOfWeek())));
                dayElement.appendChild(dayOfWeek);
                Element venueId = doc.createElement("venueId");
                venueId.appendChild(doc.createTextNode(venueIdStr));
                dayElement.appendChild(venueId);
            }
            System.out.println("Writing opening hour XML file");
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("OpeningHours.xml"));
            transformer.transform(source, result);
        } catch (ParserConfigurationException | DOMException | TransformerException e) {
        }
    }
}

package DataAccess;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Rodi on 31-Mar-17.
 */
public class ConfigParser {

    private Document document;
    private NodeList nodeList;
    private Node nNode;
    private Element eElement;

    private String venueId;
    private int weekStart;
    private int generationDay;
    private List<String> emailList = new ArrayList<>();

    public ConfigParser(){
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            File inputFile = new File("config.xml");
            document = dBuilder.parse(inputFile);
            nodeList = document.getElementsByTagName("config");
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(OpeningHourParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getVenueId(){
        System.out.println("Getting venue ID from config");
        for (int i = 0; i < nodeList.getLength(); i++) {
            nNode = nodeList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                eElement = (Element) nNode;
                venueId = eElement.getElementsByTagName("venueId").item(0).getTextContent();
            }
        }
        return venueId;
    }

    public int getEmailDay(){
        System.out.println("Getting email day from config");
        for (int i = 0; i < nodeList.getLength(); i++) {
            nNode = nodeList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                eElement = (Element) nNode;
                generationDay = Integer.parseInt(eElement.getElementsByTagName("emailDay").item(0).getTextContent());
            }
        }
        return generationDay;
    }

    public int getGenerationDay(){
        System.out.println("Getting generation day from config");
        for (int i = 0; i < nodeList.getLength(); i++) {
            nNode = nodeList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                eElement = (Element) nNode;
                weekStart = Integer.parseInt(eElement.getElementsByTagName("weekStart").item(0).getTextContent());
            }
        }
        return weekStart;
    }

    public List<String> getEmailList(){
        System.out.println("Getting a list of email addressees from config");
        for (int i = 0; i < nodeList.getLength(); i++) {
            nNode = nodeList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                eElement = (Element) nNode;
                for (int j = 0; j < eElement.getElementsByTagName("email").getLength(); j++){
                    emailList.add(eElement.getElementsByTagName("email").item(j).getTextContent());
                }
            }
        }
        return emailList;
    }
}

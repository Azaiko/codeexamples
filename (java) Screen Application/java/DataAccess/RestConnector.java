package DataAccess;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Created by Rodi on 17-Mar-17.
 */
public class RestConnector {

    private HttpResponse<String> response;

    public String getDataFromRest(String start, String end){
        try {
            System.out.println("Making connection with RESTFUL service");
            response = Unirest.post("REST URL")
                    .header("HEADER STUFF")
                    .body("BODY STUFF")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        System.out.println("Connection successful, retrieving body");
        return response.getBody();
    }
}

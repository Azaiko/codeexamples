package BusinessEntities;

import org.joda.time.LocalDate;

/**
 * Created by Rodi on 08-Mar-17.
 */
public class OpeningHour {
    private LocalDate date;
    private int openTime;
    private int closeTime;
    private int venueId;
    private int dayOfWeek;

    public OpeningHour(LocalDate date, int openTime, int closeTime, int venueId, int dayOfWeek) {
        this.date = date;
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.venueId = venueId;
        this.dayOfWeek = dayOfWeek;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getOpenTime() {
        return openTime;
    }

    public void setOpenTime(int openTime) {
        this.openTime = openTime;
    }

    public int getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(int closeTime) {
        this.closeTime = closeTime;
    }

    public int getVenueId() {
        return venueId;
    }

    public void setVenueId(int venueId) {
        this.venueId = venueId;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}

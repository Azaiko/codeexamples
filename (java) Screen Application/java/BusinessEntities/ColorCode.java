package BusinessEntities;

import java.awt.*;

/**
 * Created by Rodi on 17-Mar-17.
 */
public class ColorCode {
    private String name;
    private Color color;

    public ColorCode(String name, Color color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}

package BusinessEntities;

import org.joda.time.LocalDate;

import java.awt.*;

/**
 * Created by Rodi on 10-Mar-17.
 */
public class ColorSchedule {
    private int day;
    private int openTime;
    private int closeTime;
    private String dayOfWeekLong;
    private int time;
    private String color;
    private String colorName;
    private LocalDate date;

    public ColorSchedule(int day, int openTime, int closeTime, String dayOfWeekLong, int time, String color, String colorName, LocalDate date) {
        this.day = day;
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.dayOfWeekLong = dayOfWeekLong;
        this.time = time;
        this.color = color;
        this.colorName = colorName;
        this.date = date;
    }

    public ColorSchedule(int day, String dayOfWeekLong, LocalDate date, int openTime, int closeTime) {
        this.day = day;
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.dayOfWeekLong = dayOfWeekLong;
        this.date = date;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getOpenTime() {
        return openTime;
    }

    public void setOpenTime(int openTime) {
        this.openTime = openTime;
    }

    public int getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(int closeTime) {
        this.closeTime = closeTime;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getDayOfWeekLong() {
        return dayOfWeekLong;
    }

    public void setDayOfWeekLong(String dayOfWeekLong) {
        this.dayOfWeekLong = dayOfWeekLong;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}

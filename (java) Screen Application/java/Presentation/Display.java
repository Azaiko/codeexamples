package Presentation;

import BusinessLogic.ColorScheduleGenerator;
import BusinessLogic.EmailSender;
import DataAccess.ColorParser;
import DataAccess.ConfigParser;
import DataAccess.OpeningHourParser;
import org.joda.time.DateTime;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Rodi on 01-Mar-17.
 */
public class Display extends JFrame implements Runnable{
    private int day;
    private int hour;
    private int min;
    private int sec;

    private int minLeft;
    private int secLeft;

    private int elapsedTime = 10000;

    private DateTime dateTime;

    private JPanel clockDisplay;
    private JLabel clockLabel;
    private JLabel timerLabel;
    private JLabel imageLabel;
    private JLabel textLabel;
    private JLabel nextCol;
    private JLabel whiteSpace;

    private ColorParser colorParser = new ColorParser();
    private final ConfigParser configParser = new ConfigParser();
    private final EmailSender emailSender = new EmailSender();

    private final int weekStart = configParser.getGenerationDay();
    private final int emailDay = configParser.getEmailDay();

    private boolean email = false;

    private Color currentColor;
    private Color nextColor;
    private final Color black = new Color(0, 0, 0);
    private final Color gray = new Color(33, 33, 31);
    private final Color yellow = new Color(253, 200, 27);

    private final ImageIcon blackIcon = new ImageIcon(getClass().getResource("/images/LogoMediumBlack.png"));
    private final ImageIcon yellowIcon = new ImageIcon(getClass().getResource("/images/LogoMediumYellow.png"));

    public Display(){
        super("Roscolr");

        //Standard swing stuff
        setContentPane(clockDisplay);
        pack();
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setCursor(new java.awt.Cursor(Cursor.DEFAULT_CURSOR));

        //Make the frame fullscreen
        GraphicsEnvironment gfxEnv
                = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gfxDev = gfxEnv.getDefaultScreenDevice();
        gfxDev.setFullScreenWindow(this);

        Font clockFont = new Font("Digital-7 Mono",Font.PLAIN,225);
        Font timerFont = new Font("Digital-7 Mono",Font.PLAIN,125);
        clockLabel.setFont(clockFont);
        timerLabel.setFont(timerFont);
        nextCol.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));

        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        // Use a transparent 16 x 16 pixel cursor image.
        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
        // Create a new blank cursor.
        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                cursorImg, new Point(0, 0), "blank cursor");
        // Assign the blank cursor to the JFrame.
        setCursor(blankCursor);

        //Get the initial colors
        dateTime = new DateTime();
        day = dateTime.getDayOfWeek();
        hour = dateTime.getHourOfDay();
        changeColors();
    }

    @Override
    public void run() {
        while (true) {
            dateTime = new DateTime();

            day = dateTime.getDayOfWeek();

            hour = dateTime.getHourOfDay();
            min = dateTime.getMinuteOfHour();
            sec = dateTime.getSecondOfMinute();

            clockLabel.setText(String.format("%02d:%02d:%02d", hour, min, sec));

            minLeft = 59 - min;
            secLeft = 59 - sec;

            timerLabel.setText(String.format("00:%02d:%02d", minLeft, secLeft));

            if (min == 0 && sec <= 1){
                changeColors();
            }

            /** When the end of the week has been reached */
            if (day == weekStart && hour == 1 && min == 0 && sec <= 1){
                colorParser.transferColors();
                colorParser = new ColorParser();
            }

            /** Send email with new colors*/
            if (day == emailDay && hour == 2 && min == 0 && sec <= 1){
                email = true;
            }

            if (email == true) {
                elapsedTime++;
                if (elapsedTime > 7200) {
                    if (hostIsAvailable()) {
                        emailDay();
                        email = false;
                    } else {
                        elapsedTime = 0;
                    }
                }
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean hostIsAvailable() {
        try {
            final URL url = new URL("API CONNECTION");
            final URLConnection conn = url.openConnection();
            conn.connect();
            return true;
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            return false;
        }
    }

    public void emailDay(){

        int dow = dateTime.getDayOfWeek();

        if (weekStart > dow) dateTime = dateTime.plusDays(weekStart - dow - 1);
        else if (weekStart < dow) dateTime = dateTime.plusDays(7 - dow + weekStart - 1);
        else dateTime = dateTime.plusDays(6);

        dateTime = dateTime.plusDays(1);
        int cDay = dateTime.getDayOfMonth();
        int cMonth = dateTime.getMonthOfYear();
        int cYear = dateTime.getYear();
        dateTime = dateTime.plusDays(6);
        int dDay = dateTime.getDayOfMonth();
        int dMonth = dateTime.getMonthOfYear();
        int dYear = dateTime.getYear();

        OpeningHourParser openingHourParser = new OpeningHourParser();
        openingHourParser.generateOpeningHourXml(String.format("%02d-%02d-%02d", cYear, cMonth, cDay)
                ,String.format("%02d-%02d-%02d", dYear, dMonth, dDay));
        ColorScheduleGenerator colorScheduleGenerator = new ColorScheduleGenerator();
        colorParser.generateXml(colorScheduleGenerator.generateColorList());

        emailSender.sendMailWithAttachment("Kleurenschema " + (Integer.toString(cDay)) +
                "-" + Integer.toString(cMonth) + "-" + Integer.toString(cYear));
    }

    public void changeColors() {
        int dayOpenHour = colorParser.getOpeningHour(day);
        int dayCloseHour = colorParser.getClosingHour(day);

        if (dayOpenHour == 0 && dayCloseHour == 0){
            // Do nothing
        } else if (hour == dayOpenHour - 1){
            // HOUR BEFORE OPENING
            imageLabel.setIcon(yellowIcon);

            nextColor = colorParser.getColor(day, hour + 1);
            nextCol.setBackground(nextColor);

            clockLabel.setForeground(yellow);
            nextCol.setForeground(black);
            textLabel.setForeground(yellow);
            timerLabel.setForeground(yellow);
            textLabel.setText("Tijd tot opening");
            getContentPane().setBackground(black);
        } else if (hour == (dayCloseHour - 1)){
            // HOUR BEFORE CLOSING
            currentColor = colorParser.getColor(day, hour);

            imageLabel.setIcon(blackIcon);
            nextCol.setVisible(false);
            textLabel.setForeground(black);
            textLabel.setText("Sluitingstijd");

            clockLabel.setForeground(gray);
            getContentPane().setBackground(currentColor);
        } else if (hour >= dayOpenHour && hour < dayCloseHour) {
            // OPEN
            currentColor = colorParser.getColor(day, hour);
            nextColor = colorParser.getColor(day, hour + 1);

            imageLabel.setIcon(blackIcon);
            timerLabel.setVisible(true);
            textLabel.setVisible(true);
            nextCol.setVisible(true);
            textLabel.setForeground(black);
            textLabel.setText("Volgende sessie");

            nextCol.setBackground(nextColor);
            clockLabel.setForeground(gray);
            getContentPane().setBackground(currentColor);
        } else if (hour < dayOpenHour | hour >= dayCloseHour){
            // CLOSED
            imageLabel.setIcon(yellowIcon);
            timerLabel.setVisible(false);
            textLabel.setVisible(false);
            nextCol.setVisible(false);

            getContentPane().setBackground(black);
            clockLabel.setForeground(yellow);
        }
    }

    private void formKeyPressed(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.exit(0);
        }
    }

}
